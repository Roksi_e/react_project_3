import { RiShoppingBagFill } from "react-icons/ri";
import Btn from "../btn";
import ListEdit from "../listEdit";

function Items({
  item,
  deleteItem,
  completeItem,
  editList,
  closeList,
  saveList,
}) {
  return (
    <>
      <div className={`list-body ${item.isComplete ? "complete" : ""} `}>
        <RiShoppingBagFill className="list_icon" />
        <div className={`list-body_title ${item.isComplete ? "complete" : ""}`}>
          {item.text}
        </div>
        <Btn
          className="btn btn-light"
          value="✍"
          eventFN={() => {
            editList(item.id);
          }}
        />
        <Btn
          className="btn btn-light"
          value="❌"
          eventFN={() => {
            deleteItem(item.id);
          }}
        />
        <Btn
          className="btn btn-light"
          value="✅"
          eventFN={() => {
            completeItem(item.id);
          }}
        />{" "}
        <ListEdit item={item} closeList={closeList} saveList={saveList} />
      </div>
    </>
  );
}

export default Items;
