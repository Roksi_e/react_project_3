import Items from "../items";

import "./listBody.css";

function ListBody({
  data,
  deleteItem,
  completeItem,
  editList,
  closeList,
  saveList,
}) {
  return (
    <>
      {!data.length ? (
        <h2 className="empty">Shopping list is empty</h2>
      ) : (
        data.map((item) => {
          return (
            <Items
              item={item}
              key={item.id}
              deleteItem={deleteItem}
              completeItem={completeItem}
              editList={editList}
              closeList={closeList}
              saveList={saveList}
            />
          );
        })
      )}
    </>
  );
}

export default ListBody;
