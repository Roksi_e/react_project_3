import { Component } from "react";
import Btn from "../btn";
import ListAdd from "../listAdd";
import ListBody from "../listBoby";

import "./shopList.css";

class ShopList extends Component {
  state = {
    input: "",
    data: "",
  };

  getChange = (value) => {
    const item = {
      text: value,
      id: Math.random() * 30,
      isComplete: false,
      isEdit: false,
    };
    this.setState((state) => {
      return {
        ...state,
        data: [...state.data, item],
      };
    });
  };

  deleteItem = (id) => {
    this.setState((state) => {
      return {
        ...state,
        data: state.data.filter((item) => {
          return item.id !== id;
        }),
      };
    });
  };

  completeItem = (id) => {
    this.setState((state) => {
      return {
        ...state,
        data: state.data.map((item) => {
          return item.id === id
            ? { ...item, isComplete: !item.isComplete }
            : { ...item };
        }),
      };
    });
  };

  getEditList = (id) => {
    this.setState((state) => {
      return {
        ...state,
        data: state.data.map((item) => {
          return item.id === id
            ? { ...item, isEdit: !item.isEdit }
            : { ...item };
        }),
      };
    });
  };

  closeList = (id) => {
    this.setState((state) => {
      return {
        ...state,
        data: state.data.map((item) => {
          return item.id === id
            ? { ...item, isEdit: !item.isEdit }
            : { ...item };
        }),
      };
    });
  };

  saveList = (id, value) => {
    this.setState((state) => {
      return {
        ...state,
        data: state.data.map((item) => {
          return item.id === id
            ? { ...item, isEdit: !item.isEdit, text: value }
            : { ...item };
        }),
      };
    });
  };

  addInput = () => {
    this.setState((state) => {
      return {
        ...state,
        input: <ListAdd getChange={this.getChange} />,
      };
    });
  };

  render() {
    return (
      <>
        <div className="shop-list">
          <h1>Shopping List</h1>
          <div className="list-box">
            <Btn value="✚" className="btn btn-info" eventFN={this.addInput} />
            {this.state.input}
          </div>
          <div className="list-body_box">
            <ListBody
              data={this.state.data}
              deleteItem={this.deleteItem}
              completeItem={this.completeItem}
              editList={this.getEditList}
              closeList={this.closeList}
              saveList={this.saveList}
            />
          </div>
        </div>
      </>
    );
  }
}

export default ShopList;
