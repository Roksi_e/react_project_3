import ShopList from "../shopList";

import "./app.css";

export const App = () => {
  return (
    <div className="app">
      <ShopList />
    </div>
  );
};
