import Btn from "../btn";

import "./listEdit.css";

function ListEdit({ item, closeList, saveList }) {
  let prop = "";
  return (
    <div className={`list-edit_box ${item.isEdit ? "edit" : ""}`}>
      <Btn
        className="btn btn-danger"
        value="Close"
        eventFN={() => {
          closeList(item.id);
        }}
      />
      <input
        type="text"
        placeholder={item.text}
        className="shop-title"
        onChange={(e) => {
          prop = e.target.value;
        }}
        onBlur={(e) => {
          e.target.value = "";
        }}
      />

      <Btn
        className="btn btn-success"
        value="Save"
        eventFN={() => {
          saveList(item.id, prop);
        }}
      />
    </div>
  );
}

export default ListEdit;
