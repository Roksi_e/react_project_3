import Btn from "../btn";

import "./listAdd.css";

function ListAdd({ getChange }) {
  let prop = "";

  return (
    <>
      <input
        type="text"
        placeholder="Title"
        className="shop-title"
        onChange={(e) => {
          prop = e.target.value;
        }}
        onBlur={(e) => {
          e.target.value = "";
        }}
      />
      <Btn
        value="📀"
        className="btn btn-light"
        eventFN={() => {
          if (prop === "" || prop === undefined || prop === null) {
            return;
          } else {
            getChange(prop);
          }
        }}
      />
    </>
  );
}

export default ListAdd;
